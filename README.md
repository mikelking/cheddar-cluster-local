# Local Environment Cheddar Cluster

NOTE: This project has been recently updated to user the root branch as default in lieu of master. 


This repository reflects the files and data of the cheddar cluster as hosted on local environment. It has been structured based the trellis and bedrock framework. 

There is a detailed writeup about this project on JAFDIP => [There’s no place like 127.0.0.1/32](https://www.jafdip.com/theres-no-place-like-127-0-0-1-32/)

#### Do be certain to install the appropriate dependencies Vagrant and landrush and VirtualBox onto your vagrant system.

[Vagrant](https://www.vagrantup.com/)

[VirtualBox](https://www.virtualbox.org/)

[Composer](https://getcomposer.org/download/)

Once vagrant is installed you should install [landrush plugin](https://github.com/vagrant-landrush/landrush):
`
vagrant plugin install landrush
`

At this point you can cd or pushd into teh trellis directory and start the vagrant.
`
vagrant up
`

vagrant will install the base box and provision the internal dependencies as appropriate.

### Installing the database

In the trellis directory ssh into the local machine.

`vagrant ssh`

Then cd or pushd into the site working directory.
`pushd /srv/www/cheddar-cluster.lcl/current` 

Login into the database:
`mysql -u root -pdevpw`

Verify then drop the old cheddar-cluster_lcl_development DB
`SHOW DATABASES;
DROP DATABASE cheddar-cluster_lcl_development;
exit`

Install the new copy of the cheddar-cluster_lcl_development DB

`mysql -u root -pdevpw cheddar-cluster_lcl_development < cheddar-cluster_lcl_development.sql`

### Backing up the local
In the trellis directory ssh into the local machine.

`vagrant ssh`

Then cd or pushd into the site working directory.
`pushd /srv/www/cheddar-cluster.lcl/current` 

Dump the DB
`
mysqldump --opt -u root -pdevpw cheddar-cluster_lcl_development >cheddar-cluster_lcl_development.sql
`
## The following is presented for historical reference ONLY

In other words all you need to do is clone this repo to your local and then clone the WordPress source tree into the bedrock app space.

[Trellis Website for WordPress](https://roots.io/trellis/docs/wordpress-sites/)

[Trellis GitHub repo](https://github.com/roots/trellis)

[Documentation Index](docs/README.md)

### Usage

To properly use this repo you must follow the basic setup procedures to install trellis and bedrock onto your local system. Once Bedrock has been installed simply clone this repo in place of the app directory inside of Bedrock. Typically the path would be site/web/app. DO NOT remove the .git tree from this repo or you'll defeat the purpose.   

###### From the TRELLIS/Bedrock Docs:

The recommended directory structure for the Trellis based JAFDIP project looks like:

```shell
cheddar-cluster/  # → Root folder for the project
├── trellis/      # → Your clone of this repository
└── site/         # → A Bedrock-based WordPress site
    └── web/
        ├── app/  # → JAFDIP based WordPress content directory (themes, plugins, etc.)
        └── wp/   # → WordPress core (don't touch!)
```

See a complete working example in the [roots-example-project.com repo](https://github.com/roots/roots-example-project.com).

#### How to setup the structure

1. Create a new project directory: `$ mkdir cheddar-cluster.lcl && cd cheddar-cluster.lcl`
2. Clone Trellis: `$ git clone --depth=1 git@github.com:roots/trellis.git && rm -rf trellis/.git`
3. Clone Bedrock: `$ git clone --depth=1 git@github.com:roots/bedrock.git site && rm -rf site/.git`
4. Prepare space for HmEvents: `$ cd site/web; mv app br-app`
5. Clone App Repo: `$ git clone git@github.com:mikelking/jafdip.git app`
6. Install dependencies: `composer install` 

#### TODO

1. Include a few more steps to make the final setup of the trellis system a little easier. Most likely this wil take the for of an install shell script.
2. Add a proper bin and etc directories **DONE**
3. Install a composer manifest **DONE**
4. Implement a simple one command install/setup script _IN PROGRESS_

#### Plugins, Themes and MU-Plugins
All plugin and theme directories need to be prefixed with hm- to designate that they are haymarket developed programs. All mu-plugins need to be suffixed as -hm.php to also designate them as haymarket programs. This requirement is essential to process our development efforts through the appropriate code validation and unit testing.

Items not prefixed or suffixed as identified will be automatically excluded.   

